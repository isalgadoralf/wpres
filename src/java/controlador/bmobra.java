/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bmobra {

    /**
     * Creates a new instance of bmobra
     */
    public bmobra() {
        CargarMedida();
        CargarMobra();
    }
    private String Descripcion;
    private Double precio;
    private int mobraID;

    private Map<String, Umedida> medidas = new HashMap<String, Umedida>();
    private List<eMobra> mobras = new ArrayList<eMobra>();
    private String umedida;

    /**
     * Creates a new instance of bmaterial
     */
    private void CargarMedida() {
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
            medidas.put(element.getAbreviatura(), element);
        }

    }

    private void CargarMobra() {
        mobras.clear();
        Mobra p = new Mobra();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Mobra element = (Mobra) datos.get(i);
            eMobra x = new eMobra();
            Umedida uu = new Umedida();
            uu.setUmedidaID(element.getUmedidaID());
            uu = (Umedida) m.getObjectId(uu);
            x.setMobraID(element.getMobraID());
            x.setDescripcion(element.getDescripcion());
            x.setPrecio(element.getPrecio());
            x.setUmedida(uu.getAbreviatura());
            mobras.add(x);
        }

    }

    public void addMobra(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        //  Materiales mm = new Materiales(Descripcion, precio, 0, te.getUmedidaID());
        Mobra mo = new Mobra(Descripcion, precio, Integer.SIZE, te.getUmedidaID());
        int r = ManejadorDB.guardar(mo);

        if (r > 0) {
            CargarMobra();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public void modificar(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Mobra mm = new Mobra(Descripcion, precio, this.mobraID, te.getUmedidaID());

        int r = ManejadorDB.modificar(mm);

        if (r > 0) {
             actulizarBD();
            CargarMobra();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }
     private void actulizarBD() {
        double t;
        Item it = new Item();
        List items = ManejadorDB.getManejador().getListaItem(it, mobraID,2);
        for (int i = 0; i < items.size(); i++) {
            Deitmo m = new Deitmo();
            it = (Item) items.get(i);
            t = it.getPrecio();
            List mat = ManejadorDB.getManejador().getListaMateriales(m, it.getItemID(),2);
            double st = getsubTotal(mat);
            t = t - st;
            double stn = actulizarDeitma(mat);
            t = t +stn;
            it.setPrecio(t);
            ManejadorDB.modificar(it);
        }

    }

    private double getsubTotal(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deitmo m = (Deitmo) mts.get(i);
            s = s + m.getStotal();
        }
        return s;
    }

    private double actulizarDeitma(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deitmo m = (Deitmo) mts.get(i);
            if (m.getMobraID() == mobraID) {
                double as;
                Mobra ma = new Mobra();
                ma.setMobraID(m.getMobraID());
                ma = (Mobra) ManejadorDB.getManejador().getObjectId(ma);
                as = ma.getPrecio() * m.getCantidad();
                m.setStotal(as);
                ManejadorDB.modificarDetalle(m);
                s = s + as;
            } else {
                s = s + m.getStotal();
            }

        }
        return s;
    }


    public String deleteAction(eMobra order) {
        this.Descripcion = order.getDescripcion();
        this.precio = order.getPrecio();
        this.umedida = order.getUmedida();
        this.mobraID = order.getMobraID();
        return null;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setMobras(List<eMobra> mobras) {
        this.mobras = mobras;
    }

    public List<eMobra> getMobras() {
        return mobras;
    }

    public void setMedidas(Map<String, Umedida> medidas) {
        this.medidas = medidas;
    }

    public Map<String, Umedida> getMedidas() {
        return medidas;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public String getUmedida() {
        return umedida;
    }

}

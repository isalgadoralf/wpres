/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;
/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bequipo {

    /**
     * Creates a new instance of bequipo
     */
    private String Descripcion;
    private Double precio;
    private int equipoID;

    private Map<String, Umedida> medidas = new HashMap<String, Umedida>();
    private List<eEquipo> equipos = new ArrayList<eEquipo>();
    private String umedida;

    /**
     * Creates a new instance of bmaterial
     */
    public bequipo() {
        CargarMedida();
        CargarEquipos();
    }

    private void CargarMedida() {
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
            medidas.put(element.getAbreviatura(), element);
        }

    }

    private void CargarEquipos() {
        equipos.clear();
        Equipos p = new Equipos();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Equipos element = (Equipos) datos.get(i);
            eEquipo x = new eEquipo();
            Umedida uu = new Umedida();
            uu.setUmedidaID(element.getUmedidaID());
            uu = (Umedida) m.getObjectId(uu);
            x.setEquiposID(element.getEquiposID());
            x.setDescripcion(element.getDescripcion());
            x.setPrecio(element.getPrecio());
            x.setUmedida(uu.getAbreviatura());
            equipos.add(x);
        }

    }

    public void addEquipo(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        System.out.println(umedida);
        System.out.println(te.getDescripcionn());
        // Materiales mm = new Materiales(Descripcion, precio, 0, te.getUmedidaID());
        Equipos ep = new Equipos(Descripcion, precio, Integer.MIN_VALUE, te.getUmedidaID());
        int r = ManejadorDB.guardar(ep);

        if (r > 0) {
            CargarEquipos();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public void modificar(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Equipos mm = new Equipos(Descripcion, precio, this.equipoID, te.getUmedidaID());

        int r = ManejadorDB.modificar(mm);

        if (r > 0) {
            CargarEquipos();
            actulizarBD();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }
    private void actulizarBD() {
        double t;
        Item it = new Item();
        List items = ManejadorDB.getManejador().getListaItem(it, equipoID,3);
        for (int i = 0; i < items.size(); i++) {
            Deiteq m = new Deiteq();
            it = (Item) items.get(i);
            t = it.getPrecio();
            List mat = ManejadorDB.getManejador().getListaMateriales(m, it.getItemID(),3);
            double st = getsubTotal(mat);
            t = t - st;
            double stn = actulizarDeitma(mat);
            t = t +stn;
            it.setPrecio(t);
            ManejadorDB.modificar(it);
        }

    }

    private double getsubTotal(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deiteq m = (Deiteq) mts.get(i);
            s = s + m.getStotal();
        }
        return s;
    }

    private double actulizarDeitma(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deiteq m = (Deiteq) mts.get(i);
            if (m.getEquiposID() == equipoID) {
                double as;
                Equipos ma = new Equipos();
                ma.setEquiposID(m.getEquiposID());
                ma = (Equipos) ManejadorDB.getManejador().getObjectId(ma);
                as = ma.getPrecio() * m.getCantidad();
                m.setStotal(as);
                ManejadorDB.modificarDetalle(m);
                s = s + as;
            } else {
                s = s + m.getStotal();
            }

        }
        return s;
    }

    public String deleteAction(eEquipo order) {
        this.Descripcion = order.getDescripcion();
        this.precio = order.getPrecio();
        this.umedida = order.getUmedida();
        this.equipoID = order.getEquiposID();

        return null;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setEquipos(List<eEquipo> equipos) {
        this.equipos = equipos;
    }

    public List<eEquipo> getEquipos() {
        return equipos;
    }

    public void setMedidas(Map<String, Umedida> medidas) {
        this.medidas = medidas;
    }

    public Map<String, Umedida> getMedidas() {
        return medidas;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public String getUmedida() {
        return umedida;
    }

}

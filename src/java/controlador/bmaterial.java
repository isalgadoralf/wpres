/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bmaterial implements Serializable {

    private int materialID;
    private String Descripcion;
    private double precio;
    private eMaterial seleccion;

    private Map<String, Umedida> medidas = new HashMap<String, Umedida>();
    private List<eMaterial> materiales = new ArrayList<eMaterial>();
    private String umedida;

    /**
     * Creates a new instance of bmaterial
     */
    public bmaterial() {
        CargarMedida();
        CargarMateriales();
    }

    private void CargarMedida() {
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
            medidas.put(element.getAbreviatura(), element);
        }

    }

    private void CargarMateriales() {
        materiales.clear();
        Materiales p = new Materiales();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Materiales element = (Materiales) datos.get(i);
            eMaterial x = new eMaterial();
            Umedida uu = new Umedida();
            uu.setUmedidaID(element.getUmedidaID());
            uu = (Umedida) m.getObjectId(uu);
            x.setMaterialesID(element.getMaterialesID());
            x.setDescripcion(element.getDescripcion());
            x.setPrecio(element.getPrecio());
            x.setUmedida(uu.getAbreviatura());
            materiales.add(x);
        }

    }

    public void addMaterial(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Materiales mm = new Materiales(Descripcion, precio, 0, te.getUmedidaID());

        int r = ManejadorDB.guardar(mm);

        if (r > 0) {
            CargarMateriales();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public void modificar(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Materiales mm = new Materiales(Descripcion, precio, this.materialID, te.getUmedidaID());

        int r = ManejadorDB.modificar(mm);

        if (r > 0) {
            actulizarBD();
            CargarMateriales();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    private void actulizarBD() {
        double t;
        Item it = new Item();
        List items = ManejadorDB.getManejador().getListaItem(it, materialID,1);
        for (int i = 0; i < items.size(); i++) {
            Deitma m = new Deitma();
            it = (Item) items.get(i);
            t = it.getPrecio();
            List mat = ManejadorDB.getManejador().getListaMateriales(m, it.getItemID(),1);
            double st = getsubTotal(mat);
            t = t - st;
            double stn = actulizarDeitma(mat);
            t = t +stn;
            it.setPrecio(t);
            ManejadorDB.modificar(it);
        }

    }

    private double getsubTotal(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deitma m = (Deitma) mts.get(i);
            s = s + m.getStotal();
        }
        return s;
    }

    private double actulizarDeitma(List mts) {
        double s = 0;
        for (int i = 0; i < mts.size(); i++) {
            Deitma m = (Deitma) mts.get(i);
            if (m.getMaterialesID() == materialID) {
                double as;
                Materiales ma = new Materiales();
                ma.setMaterialesID(m.getMaterialesID());
                ma = (Materiales) ManejadorDB.getManejador().getObjectId(ma);
                as = ma.getPrecio() * m.getCantidad();
                m.setStotal(as);
                ManejadorDB.modificarDetalle(m);
                s = s + as;
            } else {
                s = s + m.getStotal();
            }

        }
        return s;
    }

    public String deleteAction(eMaterial order) {
        this.Descripcion = order.getDescripcion();
        this.precio = order.getPrecio();
        this.umedida = order.getUmedida();
        this.materialID = order.getMaterialesID();
        System.out.println("se realizo");
        System.out.println(order.getDescripcion());

        return null;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setMateriales(List<eMaterial> materiales) {
        this.materiales = materiales;
    }

    public List<eMaterial> getMateriales() {
        return materiales;
    }

    public void setMedidas(Map<String, Umedida> medidas) {
        this.medidas = medidas;
    }

    public Map<String, Umedida> getMedidas() {
        return medidas;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setMaterialID(int materialID) {
        this.materialID = materialID;
    }

    public int getMaterialID() {
        return materialID;
    }

    public eMaterial getSeleccion() {
        return seleccion;
    }

    public void setSeleccion(eMaterial seleccion) {
        this.seleccion = seleccion;
    }

}

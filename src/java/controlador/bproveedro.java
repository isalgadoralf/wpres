/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bproveedro {

    private String descripcion;
    private String Telefonos;
    private String ubicacion;
    private String categoria;
    private int proveedorID;
    private List<eProveedor> provedores = new ArrayList<eProveedor>();
    //  private List<Categoria> categorias = new ArrayList<Categoria>();
    private Map<String, Categoria> categorias = new HashMap<String, Categoria>();

    /**
     * Creates a new instance of bproveedro
     */
    public bproveedro() {
        CargarProveedores();
        CargarCategorias();

    }

    private void CargarProveedores() {
        provedores.clear();
        Proveedores p = new Proveedores();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Proveedores element = (Proveedores) datos.get(i);
            eProveedor aux =  new eProveedor(element.getDescripcion(), element.getTelefonos(), element.getUbicacion(), "", element.getProveedoresID());
            Categoria uu = new Categoria();
            uu.setCategoriaID(element.getCategoriaID());
            uu = (Categoria) m.getObjectId(uu);
            aux.setCategoria(uu.getDescripcionn());
            provedores.add(aux);
        }

    }

    private void CargarCategorias() {
        categorias.clear();
        Categoria p = new Categoria();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Categoria element = (Categoria) datos.get(i);

            categorias.put(element.getDescripcionn(), element);
        }

    }

    public void addEquipo(ActionEvent e) {
        Categoria cat = categorias.get(categoria);
        Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, cat.getCategoriaID(), Integer.MIN_VALUE);

        int r = ManejadorDB.guardar(mm);

        if (r > 0) {
            CargarProveedores();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public void modificar(ActionEvent e) {
        Categoria te = categorias.get(categoria);//  medidas.get(umedida);
        Proveedores mm = new Proveedores(descripcion, Telefonos, ubicacion, te.getCategoriaID(), this.proveedorID);

        int r = ManejadorDB.modificar(mm);

        if (r > 0) {
            //  actulizarBD();
            CargarProveedores();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public String deleteAction(eProveedor order) {
        this.descripcion = order.getDescripcion();
        this.Telefonos = order.getTelefonos();
        this.ubicacion = order.getUbicacion();
        this.categoria = order.getCategoria();
        this.proveedorID = order.getProveedorID();

        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTelefonos() {
        return Telefonos;
    }

    public void setTelefonos(String Telefonos) {
        this.Telefonos = Telefonos;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setProvedores(List<eProveedor> provedores) {
        this.provedores = provedores;
    }

    public List<eProveedor> getProvedores() {
        return provedores;
    }

   

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategorias(Map<String, Categoria> categorias) {
        this.categorias = categorias;
    }

    public Map<String, Categoria> getCategorias() {
        return categorias;
    }

    public void setProveedorID(int proveedorID) {
        this.proveedorID = proveedorID;
    }

    public int getProveedorID() {
        return proveedorID;
    }
    
}

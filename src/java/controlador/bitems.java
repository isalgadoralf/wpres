/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class bitems {

    private String Descripcion;
    private String color;
    private Double precio;
    private Map<String, Umedida> medidas = new HashMap<String, Umedida>();
    private ArrayList<String> tipos = new ArrayList<String>();
    private List<eItems> items = new ArrayList<eItems>();
    private Map<String, Categoria> categorias = new HashMap<String, Categoria>();
    private String categoria;
    private String umedida;
    private String tipo;

    /**
     * Creates a new instance of bitems
     */
    public bitems() {
        CargarMateriales();
        //CargarMedida();
        //CargarTipos();
       // CargarCategorias();
    }
    private void CargarCategorias() {
        categorias.clear();
        Categoria p = new Categoria();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Categoria element = (Categoria) datos.get(i);
         
            categorias.put(element.getDescripcionn(),element);
        }

    }

    private void CargarMedida() {
        medidas.clear();
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
            medidas.put(element.getAbreviatura(), element);
        }

    }
     private void CargarTipos() {
         tipos.add("Linea");
         tipos.add("Punto" );
         tipos.add("Area");
       

    }

    private void CargarMateriales() {
        items.clear();
        Item p = new Item();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Item element = (Item) datos.get(i);
            eItems x = new eItems();
            Umedida uu = new Umedida();
            uu.setUmedidaID(element.getUmedidaID());
            uu = (Umedida) m.getObjectId(uu);
            x.setItmesID(element.getItemID());
            x.setDescripcion(element.getDescripcion());
            x.setPrecio(element.getPrecio());
            x.setUmedida(uu.getAbreviatura());
            items.add(x);
        }

    }

    public void addObrero(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Categoria cat = categorias.get(categoria);
        // Materiales mm = new Materiales(Descripcion, precio, 0, te.getUmedidaID());
        Integer t =  tipos.indexOf(tipo);
        Item mm = new Item(Descripcion, precio, 0, te.getUmedidaID(),cat.getCategoriaID(),color,t);

        int r = ManejadorDB.guardar(mm);

        if (r > 0) {
            CargarMateriales();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setItems(List<eItems> items) {
        this.items = items;
    }

    public List<eItems> getItems() {
        return items;
    }

    public void setMedidas(Map<String, Umedida> medidas) {
        this.medidas = medidas;
    }

    public Map<String, Umedida> getMedidas() {
        return medidas;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public ArrayList<String> getTipos() {
        return tipos;
    }

    public void setTipos(ArrayList<String> tipos) {
        this.tipos = tipos;
    }

  

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCategorias(Map<String, Categoria> categorias) {
        this.categorias = categorias;
    }

    public String getCategoria() {
        return categoria;
    }

    public Map<String, Categoria> getCategorias() {
        return categorias;
    }
    
    

}

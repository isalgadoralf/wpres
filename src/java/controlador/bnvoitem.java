/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;




import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;



import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import util.Constantes;

import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Peke
 */
@ManagedBean
@ViewScoped
public class bnvoitem implements Serializable {

    private Map<String, Materiales> materiales = new HashMap<String, Materiales>();
    private Map<String, Mobra> mobras = new HashMap<String, Mobra>();
    private Map<String, Equipos> equipos = new HashMap<String, Equipos>();
    private Map<String, Umedida> medidas = new HashMap<String, Umedida>();
    private Map<String, Categoria> categorias = new HashMap<String, Categoria>();

    private List<eDetalleItem> lista = new ArrayList<eDetalleItem>();
    private List<eDetalleMobra> listamano = new ArrayList<eDetalleMobra>();
    private List<eDetalleequipo> litaequipo = new ArrayList<eDetalleequipo>();

    private ArrayList<String> tipos = new ArrayList<String>();

    private String nombreItem;
    private String material;
    private String mobra;
    private String equipo;
    private double cmaterial;
    private double cmano;
    private double cequipo;

    private String Descripcion;
    private String color;
    private Double precio;
    private String categoria;
    private String umedida;
    private String tipo;

    private double total;
    private double totalMaterial;
    private double totalMObra;
    private double totalEquipo;

    /**
     * Creates a new instance of bdetalleItem
     */
    public bnvoitem() {
        cargarMateriales();
        cargarManoObra();
        cargarEquipo();

        CargarCategorias();
        CargarMedida();
        CargarTipos();
    }

    private void cargarMateriales() {
        materiales.clear();
        Materiales p = new Materiales();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Materiales element = (Materiales) datos.get(i);
            materiales.put(element.getDescripcion(), element);
        }
    }

    private void cargarManoObra() {
        mobras.clear();
        Mobra p = new Mobra();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Mobra element = (Mobra) datos.get(i);

            mobras.put(element.getDescripcion(), element);
        }
    }

    private void cargarEquipo() {
        equipos.clear();
        Equipos p = new Equipos();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Equipos element = (Equipos) datos.get(i);

            equipos.put(element.getDescripcion(), element);
        }
    }

    public void GuardarItem(ActionEvent e) {
        Umedida te = medidas.get(umedida);
        Categoria cat = categorias.get(categoria);
        // Materiales mm = new Materiales(Descripcion, precio, 0, te.getUmedidaID());
        Integer t = tipos.indexOf(tipo);
        Item mm = new Item(Descripcion, precio, 0, te.getUmedidaID(), cat.getCategoriaID(), color, t);

        int r = ManejadorDB.guardar(mm);
        double suma = 0;

        if (r > 0) {
            //  CargarMateriales();
            ManejadorDB m = new ManejadorDB();
            Item item = (Item) m.getUltimoObject(new Item());
            suma = guardarDetalle(item);
            item.setPrecio(total);
            ManejadorDB.modificar(item);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("SE REGISTRO BIEN EL PRODUCTO " + " !"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NO SE PUEDO REGISTRAR EL  PRODUCTO  " + " !"));
        }

    }

    private double guardarDetalle(Item it) {
        double aux = 0.0;
        aux = aux + guardarDMaterial(it);
        aux = aux + guardarDManoObra(it);
        aux = aux + guardarDEquipo(it);
        return aux;
    }

    private double guardarDMaterial(Item it) {
        double aux = 0.0;
        for (int i = 0; i < lista.size(); i++) {
            eDetalleItem de = lista.get(i);
            Deitma detalle = new Deitma(de.getCantidad(), de.getMaterial().getMaterialesID(), it.getItemID(),de.getSubTotal());
            aux = aux + de.getCantidad();
            ManejadorDB.guardar(detalle);
        }

        return aux;
    }

    private double guardarDManoObra(Item it) {
        double aux = 0.0;
        for (int i = 0; i < listamano.size(); i++) {
            eDetalleMobra de = listamano.get(i);
            Deitmo detalle = new Deitmo(de.getCantidad(), de.getMano().getMobraID(), it.getItemID(),de.getSubTotal());
            aux = aux + de.getCantidad();
            ManejadorDB.guardar(detalle);
        }
        return aux;
    }

    private double guardarDEquipo(Item it) {
        double aux = 0.0;
        for (int i = 0; i < litaequipo.size(); i++) {
            eDetalleequipo de = litaequipo.get(i);
            aux = aux + de.getCantidad();
            Deiteq detalle = new Deiteq(de.getCantidad(), de.getEquipos().getEquiposID(), it.getItemID(),de.getSubTotal());
            ManejadorDB.guardar(detalle);
        }
        return aux;
    }

    public void adicionarElemento(ActionEvent event) {
        eDetalleItem e = new eDetalleItem();
        Materiales ma = materiales.get(material);
        double c = cmaterial;

        Umedida um = new Umedida();
        um.setUmedidaID(ma.getUmedidaID());
        um = (Umedida) ManejadorDB.getManejador().getObjectId(um);
        e.setUmedida(um.getAbreviatura());
        e.setCantidad(c);
        e.setMaterial(ma);

        e.setPrecio(ma.getPrecio());
        double aux;
        aux = ma.getPrecio() * e.getCantidad();
        aux = Constantes.truncateDecimal(aux, 2);
        e.setSubTotal(aux);

        total = total + aux;
        totalMaterial = totalMaterial + aux;

        lista.add(e);

    }

    public void adicionarMobra(ActionEvent event) {

        eDetalleMobra e = new eDetalleMobra();
        Mobra ma = mobras.get(mobra);
        double c = cmano;

        Umedida um = new Umedida();
        um.setUmedidaID(ma.getUmedidaID());
        um = (Umedida) ManejadorDB.getManejador().getObjectId(um);
        e.setUmedida(um.getAbreviatura());
        e.setCantidad(c);
        e.setMano(ma);
        e.setPrecio(ma.getPrecio());

        double aux;
        aux = ma.getPrecio() * e.getCantidad();
        aux = Constantes.truncateDecimal(aux, 2);
        e.setSubTotal(aux);

        total = total + aux;
        totalMObra = totalMObra + aux;
        listamano.add(e);

    }

    public void adicionarEquipo(ActionEvent event) {
        eDetalleequipo e = new eDetalleequipo();
        Equipos ma = equipos.get(equipo);
        double c = cequipo;

        Umedida um = new Umedida();
        um.setUmedidaID(ma.getUmedidaID());
        um = (Umedida) ManejadorDB.getManejador().getObjectId(um);
        e.setUmedida(um.getAbreviatura());
        e.setCantidad(c);
        e.setEquipos(ma);
        e.setPrecio(ma.getPrecio());
        double aux;
        aux = ma.getPrecio() * e.getCantidad();
        aux = Constantes.truncateDecimal(aux, 2);
        e.setSubTotal(aux);

        total = total + aux;
        totalEquipo = totalEquipo + aux;

        litaequipo.add(e);

    }

    private void CargarCategorias() {
        categorias.clear();
        Categoria p = new Categoria();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Categoria element = (Categoria) datos.get(i);

            categorias.put(element.getDescripcionn(), element);
        }

    }

    private void CargarMedida() {
        medidas.clear();
        Umedida p = new Umedida();
        ManejadorDB m = new ManejadorDB();
        List datos = m.getLista(p);
        for (int i = 0; i < datos.size(); i++) {
            Umedida element = (Umedida) datos.get(i);
            medidas.put(element.getAbreviatura(), element);
        }

    }

    private void CargarTipos() {
        tipos.add("Linea");
        tipos.add("Punto");
        tipos.add("Area");
        tipos.add("Cubo");

    }

    public String deleteAction(eDetalleItem order) {

        boolean sw = true;
        int i = 0;
        while (sw && i < lista.size()) {
            eDetalleItem e = lista.get(i);

            if (order.equals(e)) {
                double aux;
                aux = e.getMaterial().getPrecio() * e.getCantidad();

                total = total - aux;
                totalMaterial = totalMaterial - aux;
                lista.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }

    public String deleteActionMano(eDetalleMobra order) {
        System.out.println("paso por aki");
        boolean sw = true;
        int i = 0;
        while (sw && i < listamano.size()) {
            eDetalleMobra e = listamano.get(i);

            if (order.equals(e)) {
                double aux;
                aux = e.getMano().getPrecio() * e.getCantidad();
                total = total - aux;
                totalMObra = totalMObra - aux;
                listamano.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }

    public String deleteActionEquipo(eDetalleequipo order) {

        boolean sw = true;
        int i = 0;
        while (sw && i < litaequipo.size()) {
            eDetalleequipo e = litaequipo.get(i);

            if (order.equals(e)) {
                double aux;
                aux = e.getEquipos().getPrecio() * e.getCantidad();
                total = total - aux;
                totalEquipo = totalEquipo - aux;
                litaequipo.remove(i);
                sw = !sw;
            }

            i++;
        }
        return null;
    }

    public Map<String, Materiales> getMateriales() {
        return materiales;
    }

    public void setMateriales(Map<String, Materiales> materiales) {
        this.materiales = materiales;
    }

    public void setEquipos(Map<String, Equipos> equipos) {
        this.equipos = equipos;
    }

    public void setMobras(Map<String, Mobra> mobras) {
        this.mobras = mobras;
    }

    public Map<String, Equipos> getEquipos() {
        return equipos;
    }

    public Map<String, Mobra> getMobras() {
        return mobras;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMobra() {
        return mobra;
    }

    public void setMobra(String mobra) {
        this.mobra = mobra;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public double getCmaterial() {
        return cmaterial;
    }

    public void setCmaterial(double cmaterial) {
        this.cmaterial = cmaterial;
    }

    public double getCmano() {
        return cmano;
    }

    public void setCmano(double cmano) {
        this.cmano = cmano;
    }

    public double getCequipo() {
        return cequipo;
    }

    public void setCequipo(double cequipo) {
        this.cequipo = cequipo;
    }

    public void setLista(List<eDetalleItem> lista) {
        this.lista = lista;
    }

    public List<eDetalleItem> getLista() {
        return lista;
    }

    public List<eDetalleMobra> getListamano() {
        return listamano;
    }

    public void setListamano(List<eDetalleMobra> listamano) {
        this.listamano = listamano;
    }

    public List<eDetalleequipo> getLitaequipo() {
        return litaequipo;
    }

    public void setLitaequipo(List<eDetalleequipo> litaequipo) {
        this.litaequipo = litaequipo;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public Map<String, Umedida> getMedidas() {
        return medidas;
    }

    public void setMedidas(Map<String, Umedida> medidas) {
        this.medidas = medidas;
    }

    public Map<String, Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(Map<String, Categoria> categorias) {
        this.categorias = categorias;
    }

    public ArrayList<String> getTipos() {
        return tipos;
    }

    public void setTipos(ArrayList<String> tipos) {
        this.tipos = tipos;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public double getTotalMaterial() {
        return totalMaterial;
    }

    public void setTotalMaterial(double totalMaterial) {
        this.totalMaterial = totalMaterial;
    }

    public double getTotalMObra() {
        return totalMObra;
    }

    public void setTotalMObra(double totalMObra) {
        this.totalMObra = totalMObra;
    }

    public double getTotalEquipo() {
        return totalEquipo;
    }

    public void setTotalEquipo(double totalEquipo) {
        this.totalEquipo = totalEquipo;
    }

}

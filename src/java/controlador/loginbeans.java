/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import Modelo.model.*;
import Modelo.datos.ManejadorDB;
import Modelo.estructura.*;

/**
 *
 * @author Rafael
 */
@ManagedBean
@ViewScoped
public class loginbeans implements Serializable {

    private String login;
    private String pass;

    /**
     * Creates a new instance of loginbeans
     */
    public void autentificar() {

        if (login.equals("RAFA")) {

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("vista/vItems.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(loginbeans.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuario no registrado" + " !"));
            //  FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
        }

    }

    public void logout() {
        try {
            FacesContext.getCurrentInstance().getExternalContext()
                    .invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect("http://52.40.74.203:8080/WPres/");
        } catch (IOException ex) {
            Logger.getLogger(loginbeans.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

}

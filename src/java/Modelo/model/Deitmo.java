package Modelo.model;

public class Deitmo {

    private Double cantidad;
    private Integer mobraID;
    private Integer itemID;
    private Double stotal;

    public Deitmo( ) { 
      }

    public Deitmo(Double cantidad, Integer mobraID, Integer itemID, Double stotal) {
        this.cantidad = cantidad;
        this.mobraID = mobraID;
        this.itemID = itemID;
        this.stotal = stotal;
    }
   
    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getMobraID() {
        return mobraID;
    }

    public void setMobraID(Integer mobraID) {
        this.mobraID = mobraID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public void setStotal(Double stotal) {
        this.stotal = stotal;
    }

    public Double getStotal() {
        return stotal;
    }
    

}
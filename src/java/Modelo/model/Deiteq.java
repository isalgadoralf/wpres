package Modelo.model;

public class Deiteq {

    private Double cantidad;
    private Integer equiposID;
    private Integer itemID;
    private Double stotal;

    public Deiteq() {
    }

    public Deiteq(Double cantidad, Integer equiposID, Integer itemID, Double stotal) {
        this.cantidad = cantidad;
        this.equiposID = equiposID;
        this.itemID = itemID;
        this.stotal = stotal;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getEquiposID() {
        return equiposID;
    }

    public void setEquiposID(Integer equiposID) {
        this.equiposID = equiposID;
    }

    public Integer getItemID() {
        return itemID;
    }

    public void setItemID(Integer itemID) {
        this.itemID = itemID;
    }

    public void setStotal(Double stotal) {
        this.stotal = stotal;
    }

    public Double getStotal() {
        return stotal;
    }
    
}

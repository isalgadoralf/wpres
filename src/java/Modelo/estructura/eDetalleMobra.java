/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.estructura;

import Modelo.*;
import Modelo.model.Mobra;


/**
 *
 * @author Peke
 */
public class eDetalleMobra {

    private Mobra mano;
    private String umedida;
    private double precio;
    private double cantidad;
    private double subTotal;

    public eDetalleMobra() {
    }

    public eDetalleMobra(Mobra mano, String umedida, double cantidad) {
        this.mano = mano;
        this.umedida = umedida;
        this.cantidad = cantidad;
    }

    public void setMano(Mobra mano) {
        this.mano = mano;
    }

    public Mobra getMano() {
        return mano;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.estructura;


import Modelo.model.Equipos;

/**
 *
 * @author Peke
 */

public class eDetalleequipo {
    private Equipos equipos;
    private String umedida;
    private double precio;
    private double cantidad;
    private double subTotal;

    public eDetalleequipo() {
    }

    public eDetalleequipo(Equipos equipos, String umedida, double cantidad) {
        this.equipos = equipos;
        this.umedida = umedida;
        this.cantidad = cantidad;
    }

    public void setEquipos(Equipos equipos) {
        this.equipos = equipos;
    }

    public Equipos getEquipos() {
        return equipos;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }
    
    
    
}


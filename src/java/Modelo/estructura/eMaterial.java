/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.estructura;



/**
 *
 * @author Rafael
 */
public class eMaterial {
    private Integer materialesID;
    private String Descripcion;
    private double precio;
    private String umedida;

    public eMaterial() {
    }

    public eMaterial(Integer materialesID, String Descripcion, double precio, String umedida) {
        this.materialesID = materialesID;
        this.Descripcion = Descripcion;
        this.precio = precio;
        this.umedida = umedida;
    }

    

   

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

   
    

    public String getUmedida() {
        return umedida;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setMaterialesID(Integer materialesID) {
        this.materialesID = materialesID;
    }

    public Integer getMaterialesID() {
        return materialesID;
    }
    
    
}

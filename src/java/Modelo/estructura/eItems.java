/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.estructura;

/**
 *
 * @author Rafael
 */
public class eItems {

    private Integer itmesID;
    private String Descripcion;
    private Double precio;
    private String umedida;

    public eItems() {
    }

    public eItems(Integer itmesID, String Descripcion, Double precio, String umedida) {
        this.itmesID = itmesID;
        this.Descripcion = Descripcion;
        this.precio = precio;
        this.umedida = umedida;
    }

    public void setItmesID(Integer itmesID) {
        this.itmesID = itmesID;
    }

    public Integer getItmesID() {
        return itmesID;
    }

   
    

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }
    
    
}

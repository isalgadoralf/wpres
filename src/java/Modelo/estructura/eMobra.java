/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.estructura;

/**
 *
 * @author Rafael
 */
public class eMobra {
    private Integer mobraID;
    private String descripcion;
    private Double precio;
    private String umedida;

    public eMobra(Integer mobraID, String descripcion, Double precio, String umedida) {
        this.mobraID = mobraID;
        this.descripcion = descripcion;
        this.precio = precio;
        this.umedida = umedida;
    }

    public eMobra() {
    }

    public Integer getMobraID() {
        return mobraID;
    }

    public void setMobraID(Integer mobraID) {
        this.mobraID = mobraID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.estructura;

import Modelo.model.Materiales;



/**
 *
 * @author Peke
 */
public class eDetalleItem {
    private Materiales material;
    private String umedida;
    private double precio;
    private double cantidad;
    private double subTotal;

    public eDetalleItem() {
    }

    public eDetalleItem(Materiales material, String umedida, double cantidad) {
        this.material = material;
        this.umedida = umedida;
        this.cantidad = cantidad;
    }

    public Materiales getMaterial() {
        return material;
    }

    public void setMaterial(Materiales material) {
        this.material = material;
    }

    public String getUmedida() {
        return umedida;
    }

    public void setUmedida(String umedida) {
        this.umedida = umedida;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getPrecio() {
        return precio;
    }

    public double getSubTotal() {
        return subTotal;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.estructura;
/**
 *
 * @author Rafael
 */
public class eProveedor {

    private String descripcion;
    private String Telefonos;
    private String ubicacion;
    private String categoria;
    private int proveedorID;

    public eProveedor() {
    }
    
    public eProveedor(String descripcion, String Telefonos, String ubicacion, String categoria, int proveedorID) {
        this.descripcion = descripcion;
        this.Telefonos = Telefonos;
        this.ubicacion = ubicacion;
        this.categoria = categoria;
        this.proveedorID = proveedorID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTelefonos() {
        return Telefonos;
    }

    public void setTelefonos(String Telefonos) {
        this.Telefonos = Telefonos;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getProveedorID() {
        return proveedorID;
    }

    public void setProveedorID(int proveedorID) {
        this.proveedorID = proveedorID;
    }
    

}
